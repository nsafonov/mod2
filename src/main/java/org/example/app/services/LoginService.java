package org.example.app.services;

import org.apache.log4j.Logger;
import org.example.web.dto.LoginForm;
import org.example.web.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    private Logger logger = Logger.getLogger(LoginService.class);
    private ProjectRepository<User> userRepository;

    @Autowired
    public LoginService(ProjectRepository<User> userRepository) {
        this.userRepository = userRepository;
    }

    public boolean authenticate(LoginForm loginFrom) {
        logger.info("try auth with user-form: " + loginFrom);

        for (User user : userRepository.retrieveAll()) {
            if (loginFrom.getUsername().equals(user.getUsername()) && loginFrom.getPassword().equals(user.getPassword())) {
                return true;
            }
        }

        return false;
    }
}
