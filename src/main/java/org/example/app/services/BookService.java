package org.example.app.services;

import org.example.web.dto.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class BookService {

    private final ProjectRepository<Book> bookRepo;

    @Autowired
    public BookService(ProjectRepository<Book> bookRepo) {
        this.bookRepo = bookRepo;
    }

    public List<Book> getAllBooks() {
        return bookRepo.retrieveAll();
    }

    public List<Book> getBooksFilteredBySize(Integer size) {
        return bookRepo.retrieveAll().stream()
                .filter(book -> size == null || book.getSize().equals(size))
                .collect(Collectors.toList());
    }

    public List<Book> getBooksFilteredByAuthor(String author) {
        return bookRepo.retrieveAll().stream()
                .filter(book -> author.isEmpty() || book.getAuthor().matches(author))
                .collect(Collectors.toList());
    }

    public List<Book> getBooksFilteredByTitle(String title) {
        return bookRepo.retrieveAll().stream()
                .filter(book -> title.isEmpty() || book.getAuthor().matches(title))
                .collect(Collectors.toList());
    }

    public void saveBook(Book book) {
        bookRepo.store(book);
    }

    public boolean removeBookById(Integer bookIdToRemove) {
        return bookRepo.removeItemById(bookIdToRemove);
    }

    public void removeBookBySize(Integer bookSize) {
        for (Book book : bookRepo.retrieveAll()) {
            if (book.getSize().equals(bookSize)) {
                bookRepo.removeItemById(book.getId());
            }
        }
    }

    public void removeBookByTitleRegEx(String bookTitleRegEx) {
        for (Book book : bookRepo.retrieveAll()) {
            if (book.getTitle().matches(bookTitleRegEx)) {
                bookRepo.removeItemById(book.getId());
            }
        }
    }

    public void removeBookByAuthorRegEx(String bookAuthorRegExp) {
        for (Book book : bookRepo.retrieveAll()) {
            if (book.getAuthor().matches(bookAuthorRegExp)) {
                bookRepo.removeItemById(book.getId());
            }
        }
    }
}
