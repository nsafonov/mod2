package org.example.app.services;

import org.apache.log4j.Logger;
import org.example.web.dto.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository implements ProjectRepository<User> {

    private final Logger logger = Logger.getLogger(UserRepository.class);
    private final List<User> repo = new ArrayList<>();

    public UserRepository() {
        User root = new User();
        root.setUsername("root");
        root.setPassword("123");
        repo.add(root);
    }

    @Override
    public List<User> retrieveAll() {
        return repo;
    }

    @Override
    public void store(User user) {
        user.setId(user.hashCode());
        logger.info("store new user " + user);
        repo.add(user);
    }

    @Override
    public boolean removeItemById(Integer userId) {
        return false;
    }
}
