package org.example.web.controllers;

import org.apache.log4j.Logger;
import org.example.app.services.SignupService;
import org.example.web.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class SignupController {

    private final Logger logger = Logger.getLogger(SignupController.class);
    private final SignupService signupService;

    @Autowired
    public SignupController(SignupService signupService) {
        this.signupService = signupService;
    }

    @GetMapping("/signup")
    public String signup(Model model) {
        model.addAttribute("user", new User());
        return "signup_page";
    }

    @PostMapping("/signup")
    public String createUser(User user) {
        logger.info("signing up user " + user.getUsername());
        signupService.signup(user);
        return "redirect:/login";
    }
}
