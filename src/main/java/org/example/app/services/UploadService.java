package org.example.app.services;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UploadService {

    private final File uploadsDirectory;

    public UploadService(String uploadsDirectory) {
        this.uploadsDirectory = new File(uploadsDirectory);
    }

    @PostConstruct
    public void init() {
        if (!uploadsDirectory.exists()) {
            uploadsDirectory.mkdirs();
        }
    }

    public void saveFile(String fileName, byte[] contents) throws IOException {
        FileUtils.writeByteArrayToFile(getFileByName(fileName), contents);
    }

    public List<String> getFiles() throws IOException {
        try (Stream<Path> paths = Files.walk(Paths.get(uploadsDirectory.getAbsolutePath()))) {
            return paths
                    .filter(Files::isRegularFile)
                    .map(path -> path.getFileName().toString())
                    .collect(Collectors.toList());
        }
    }

    public File getFileByName(String fileName) {
        return new File(uploadsDirectory.getAbsolutePath() + File.separator + fileName);
    }

}
