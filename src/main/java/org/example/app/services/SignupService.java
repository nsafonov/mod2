package org.example.app.services;

import org.example.web.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SignupService {

    private final ProjectRepository<User> userRepository;

    @Autowired
    public SignupService(ProjectRepository<User> userRepository) {
        this.userRepository = userRepository;
    }

    public void signup(User user) {
        userRepository.store(user);
    }

}
