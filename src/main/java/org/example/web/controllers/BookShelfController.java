package org.example.web.controllers;

import org.apache.log4j.Logger;
import org.example.app.services.BookService;
import org.example.app.services.UploadService;
import org.example.web.dto.*;
import org.example.web.exceptions.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;

@Controller
@RequestMapping(value = "/books")
public class BookShelfController {

    private final Logger logger = Logger.getLogger(BookShelfController.class);
    private final BookService bookService;
    private final UploadService uploadService;

    @Autowired
    public BookShelfController(BookService bookService, UploadService uploadService) {
        this.bookService = bookService;
        this.uploadService = uploadService;
    }

    @GetMapping("/shelf")
    public String books(Model model) throws IOException {
        logger.info("got book shelf");

        model.addAttribute("book", new Book());
        model.addAttribute("bookIdToRemove", new BookIdToRemove());
        model.addAttribute("bookSizeToRemove", new BookSize());
        model.addAttribute("bookTitleToRemove", new BookTitle());
        model.addAttribute("bookAuthorToRemove", new BookAuthor());
        model.addAttribute("bookSizeToFilter", new BookSize());
        model.addAttribute("bookTitleToFilter", new BookTitle());
        model.addAttribute("bookAuthorToFilter", new BookAuthor());
        model.addAttribute("bookList", bookService.getAllBooks());
        model.addAttribute("files", uploadService.getFiles());

        return "book_shelf";
    }

    @PostMapping("/save")
    public String saveBook(@Valid Book book, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("book", book);
            model.addAttribute("bookIdToRemove", new BookIdToRemove());
            model.addAttribute("bookSizeToRemove", new BookSize());
            model.addAttribute("bookTitleToRemove", new BookTitle());
            model.addAttribute("bookAuthorToRemove", new BookAuthor());
            model.addAttribute("bookSizeToFilter", new BookSize());
            model.addAttribute("bookTitleToFilter", new BookTitle());
            model.addAttribute("bookAuthorToFilter", new BookAuthor());
            model.addAttribute("bookList", bookService.getAllBooks());
            return "book_shelf";
        }

        bookService.saveBook(book);
        logger.info("current repository size: " + bookService.getAllBooks().size());
        return "redirect:/books/shelf";
    }

    @ExceptionHandler(FileUploadException.class)
    public String handleFileUploadError(Model model, FileUploadException exception) {
        model.addAttribute("errorMessage", exception.getMessage());
        return "errors/500";
    }

    @PostMapping("/uploadFile")
    public String upload(@RequestParam MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new FileUploadException("no file provided for uploading");
        }

        uploadService.saveFile(file.getOriginalFilename(), file.getBytes());

        logger.info("new file is saved");
        return "redirect:/books/shelf";
    }

    @GetMapping(value = "/download/{fileName:.+}")
    @ResponseBody
    public FileSystemResource download(@PathVariable String fileName, HttpServletResponse response) {
        File file = uploadService.getFileByName(fileName);
        logger.info("downloading file '" + fileName + "' from " + file.getAbsolutePath());

        response.setHeader("Content-Disposition", "attachment; filename="+file.getName());
        return new FileSystemResource(file);
    }

    @PostMapping("/remove")
    public String removeBookById(@Valid BookIdToRemove bookIdToRemove, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("book", new Book());
            model.addAttribute("bookList", bookService.getAllBooks());
            model.addAttribute("bookIdToRemove", bookIdToRemove);
            model.addAttribute("bookSizeToRemove", new BookSize());
            model.addAttribute("bookTitleToRemove", new BookTitle());
            model.addAttribute("bookAuthorToRemove", new BookAuthor());
            model.addAttribute("bookSizeToFilter", new BookSize());
            model.addAttribute("bookTitleToFilter", new BookTitle());
            model.addAttribute("bookAuthorToFilter", new BookAuthor());
            return "book_shelf";
        }

        logger.info("about to remove " + bookIdToRemove.getId());
        bookService.removeBookById(bookIdToRemove.getId());
        return "redirect:/books/shelf";
    }

    @PostMapping("/removeBySize")
    public String removeBookBySize(@Valid @ModelAttribute("bookSizeToRemove") BookSize bookSize, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("book", new Book());
            model.addAttribute("bookList", bookService.getAllBooks());
            model.addAttribute("bookIdToRemove", new BookIdToRemove());
            model.addAttribute("bookTitleToRemove", new BookTitle());
            model.addAttribute("bookAuthorToRemove", new BookAuthor());
            model.addAttribute("bookSizeToFilter", new BookSize());
            model.addAttribute("bookTitleToFilter", new BookTitle());
            model.addAttribute("bookAuthorToFilter", new BookAuthor());
            return "book_shelf";
        }

        bookService.removeBookBySize(bookSize.getSize());
        return "redirect:/books/shelf";
    }

    @PostMapping("/removeByTitle")
    public String removeBookByTitle(@Valid @ModelAttribute("bookTitleToRemove") BookTitle bookTitleToRemove, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("book", new Book());
            model.addAttribute("bookList", bookService.getAllBooks());
            model.addAttribute("bookIdToRemove", new BookIdToRemove());
            model.addAttribute("bookSizeToRemove", new BookSize());
            model.addAttribute("bookAuthorToRemove", new BookAuthor());
            model.addAttribute("bookSizeToFilter", new BookSize());
            model.addAttribute("bookTitleToFilter", new BookTitle());
            model.addAttribute("bookAuthorToFilter", new BookAuthor());
            return "book_shelf";
        }

        bookService.removeBookByTitleRegEx(bookTitleToRemove.getTitle());
        return "redirect:/books/shelf";
    }

    @PostMapping("/removeByAuthor")
    public String removeBookByAuthor(@Valid @ModelAttribute("bookAuthorToRemove") BookAuthor bookAuthorToRemove, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("book", new Book());
            model.addAttribute("bookList", bookService.getAllBooks());
            model.addAttribute("bookIdToRemove", new BookIdToRemove());
            model.addAttribute("bookSizeToRemove", new BookSize());
            model.addAttribute("bookTitleToRemove", new BookTitle());
            model.addAttribute("bookSizeToFilter", new BookSize());
            model.addAttribute("bookTitleToFilter", new BookTitle());
            model.addAttribute("bookAuthorToFilter", new BookAuthor());
            return "book_shelf";
        }

        bookService.removeBookByAuthorRegEx(bookAuthorToRemove.getAuthor());
        return "redirect:/books/shelf";
    }

    @PostMapping("/filterBySize")
    public String filterBooksBySize(@Valid @ModelAttribute("bookSizeToFilter") BookSize bookSizeToFilter, BindingResult bindingResult, Model model) {
        model.addAttribute("book", new Book());
        model.addAttribute("bookList", bookService.getBooksFilteredBySize(bookSizeToFilter.getSize()));
        model.addAttribute("bookIdToRemove", new BookIdToRemove());
        model.addAttribute("bookSizeToRemove", new BookSize());
        model.addAttribute("bookTitleToRemove", new BookTitle());
        model.addAttribute("bookAuthorToRemove", new BookAuthor());
        model.addAttribute("bookTitleToFilter", new BookTitle());
        model.addAttribute("bookAuthorToFilter", new BookAuthor());

        return "book_shelf";
    }

    @PostMapping("/filterByTitle")
    public String filterBooksByTitle(@Valid @ModelAttribute("bookTitleToFilter") BookTitle bookTitleToFilter, BindingResult bindingResult, Model model) {
        model.addAttribute("book", new Book());
        model.addAttribute("bookList", bookService.getBooksFilteredByTitle(bookTitleToFilter.getTitle()));
        model.addAttribute("bookIdToRemove", new BookIdToRemove());
        model.addAttribute("bookSizeToRemove", new BookSize());
        model.addAttribute("bookTitleToRemove", new BookTitle());
        model.addAttribute("bookAuthorToRemove", new BookAuthor());
        model.addAttribute("bookSizeToFilter", new BookSize());
        model.addAttribute("bookAuthorToFilter", new BookAuthor());

        return "book_shelf";
    }

    @PostMapping("/filterByAuthor")
    public String filterBooksByAuthor(@Valid @ModelAttribute("bookAuthorToFilter") BookAuthor bookAuthorToFilter, BindingResult bindingResult, Model model) {
        model.addAttribute("book", new Book());
        model.addAttribute("bookList", bookService.getBooksFilteredByAuthor(bookAuthorToFilter.getAuthor()));
        model.addAttribute("bookIdToRemove", new BookIdToRemove());
        model.addAttribute("bookSizeToRemove", new BookSize());
        model.addAttribute("bookTitleToRemove", new BookTitle());
        model.addAttribute("bookAuthorToRemove", new BookAuthor());
        model.addAttribute("bookSizeToFilter", new BookSize());
        model.addAttribute("bookTitleToFilter", new BookTitle());

        return "book_shelf";
    }

}
