package org.example.app.config;

import org.example.app.services.UploadService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Configuration
@ComponentScan(basePackages = "org.example.app")
public class AppContextConfig {

    @Bean
    public UploadService uploadService() {
        String uploadDirectory = System.getProperty("catalina.home") + File.separator + "external_uploads";
        return new UploadService(uploadDirectory);
    }

}
